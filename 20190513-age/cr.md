# Présent·es

- Milouse
- Kolbo5
- Sacha

# Déroulé de l'Assemblée Générale

L'Assemblée Générale Extraordinaire (AGE) débute à 18h00.

Milouse a procédé au rappel de l'ordre du jour, comme suit :

- Ouverture de l'Assemblée
- Bilan moral
- Bilan financier
- Annonce du budget prévisionnel
- Fixation du montant de la cotisation
- Renouvellement du bureau
- Modification des statuts
- Place de l'association au sein du collectif CHATONS

## Ouverture de l'Assemblée

Milouse rappelle que cette présente Assemblée a été convoquée suite à la
constatation de la non-tenue de l'AGO dans les 404 jours définie dans
les statuts. Il rappelle que le but de cette AGE est de maintenir
l'association en simplifiant son fonctionnement ainsi que ses statuts.

## Rapport moral de l'association

Le rapport moral de cette année couvre toute la période de mars 2018 à
mai 2019.

L'association est principalement en sommeil faute de motivation et de
disponibilité de la plupart de ses membres, qui l'ont progressivement
quitté. Suite à ces départs, les serveurs se sont retrouvés dans un état
rendant leur exploitation impossible ou presque à maintenir.

Afin de reprendre la main sur l'existant et simplifier le fonctionnement
interne, Milouse a choisi de migrer l'instance Nextcloud chez
l'hébergeur Hetzner. En parallèle il a procédé à la réinstallation du
serveur hébergé chez FAIMaison. Le serveur chez FAIMaison est encore
vide à ce jour.

Il a été convenu qu'au vu des disponibilité de chacun, il était
illusoire d'imaginer un investissement des membres en dehors de temps
conviviaux. Il a également été constaté que le mieux, pour la santé de
chacun et le devenir de l'association, était de privilégier ces temps
conviviaux qu'un engagement technique intenable. C'est ainsi que les
membres Kolbo, Sacha et Milouse se sont régulièrement rencontrés sur
cette période pour des temps conviviaux et à la marge quelques actions
techniques.

### Vote

Le rapport moral est soumis au vote.

Sur les trois personnes présentes :

- 3 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Bilan financier

Kolbo5 commence la présentation du rapport financier. Sa présentation
utilise le tableau visible dans la [figure 1][financial results] comme
support.

![Résultat financier sous forme de tableau][financial results]

[financial results]: ./resultat_duchesse.png

Le rapport est fait sur l'année calendaire (jusqu'au 31 décembre 2018).

Aujourd'hui l'association a 167€63 sur le compte, sachant qu'il faudra
peut-être rembourser dans les 80€ à Gordon dont le virement automatique
est toujours actif alors qu'il ne profite plus des services.

Il est également à noter que l'association n'a pour l'instant souscrit à
aucune assurance.


### Vote

Le bilan financier est soumis au vote.

Sur les trois personnes présentes :

- 3 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Budget prévisionnel

- L'association paie 16 € par an d'adhésion à FAIMaison.
- L'association paie 30 € par mois la location d'un espace 1U à
  FAIMaison (soit 360 € par an).
- L'association paie 7,06 € par mois à Hetzner pour l'instance Nextcloud (soit 84,72 € par an)
Cela fait un prévisionnel de 460,72 € de charges.

En entrée, en ne considérant que les trois adhérents actuels, l'association perçois :
- 3 adhésion (soit 60 €) ;
- entre 6 et 10 € mois pour l'accès au service, soit 264 € par an.
Soit un prévisionnel de produit de 324 €.

L'association serait déficitaire sur l'année 2019 de 216,72 €.
Il faudra donc dans le courant de l'année prévoir :
- une baisse de nos charges (en arrétant Hetzner et/ou en diminuant le payment pour le U à FAIMaison)
- et/ou une augmentation de nos produits (Don des membres, augmentation des participation au service, nouveau membres, ...)

## Montant de la cotisation 2019

Le montant de l'adhésion est fixé à 20 € par an.

### Vote

La conservation de la cotisation annuelle à 20 € est soumise au vote.

Sur les trois personnes présentes :

- 3 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Renouvellement du bureau

La composition du bureau existant est rappelée, d'après la déclaration
en préfecture. Les personnes suivantes en faisaient partie :

- Milouse
- Kolbo

Candidat·es pour le nouveau bureau :

- Présidence : Étienne Deparis (dit Milouse)
- Trésorerie : Emmanuel Olivier (dit Kolbo5)

### Vote pour l'élection d'Étienne Deparis au poste de président

Sur les trois personnes présentes :

- 3 votes pour
- 0 vote contre
- 0 votes blancs ou abstentions

### Vote pour l'élection d'Emmanuel Olivier au poste de trésorier

Sur les trois personnes présentes :

- 3 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Modification des statuts

Les statuts ont été modifiés pendant cette AGE. À retenir :

- suppression du statut du Matou ;
- relocalisation de l'association au domicile du président :
  Étienne Deparis, 3 rue des Noisetiers, 44140 GENESTON ;
- suppression du règlement intérieur.

### Vote pour la modification des statuts

Sur les trois personnes présentes :

- 3 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Place de l'association au sein du collectif CHATONS

Milouse annonce que l'association Framasoft, actuellement en position de
"direction tournante" du collectif CHATONS, mène une grande enquête sur
la santé du collectif. Chaque entité est ainsi invitée à répondre à un
long questionnaire.

Au vu du bilan moral de cette année, Milouse propose de profiter de
l'occasion pour officiellement annoncer au reste du collectif l'état "en
(re)construction" de Duchesse. Il justifie ce changement d'état pour
améliorer la vision que les personnes externes pourraient avoir de
l'association Duchesse : l'association n'est pas en état d'accueillir de
nouvelles personnes strictement consommatrices. Elle reste cependant
ouverte à l'accueil de nouveaux ou nouvelles membres prêt·e·s à
s'investir.

### Vote sur le changement d'état de l'assocition

L'envoi d'une communication formelle sur la *mailing list* officielle
des CHATONS concernant l'état "en construction" de Duchesse est soumis
au vote.

Sur les trois personnes présentes :

- 3 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

# Clôture de l'Assemblée

L'Assemblée Générale Extraordinaire se termine à 19h03.
