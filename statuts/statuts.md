# Constitution

## Article 1 - Constitution

Il est fondé entre les adhérents aux présents statuts une association
régie par la loi du 1er juillet 1901 modifiée et ses textes
d’application. La durée de l'Association est illimitée.

## Article 2 - Dénomination

L'association a pour nom "Duchesse, CHATONS Nantais".

## Article 3 - Objet

L'association a pour objet désintéressé et non lucratif :

- de permettre la rencontre de personne souhaitant échanger sur
  l'auto-hébergement et la décentralisation du Web ;
- de proposer des services d'hébergement informatiques basés sur des
  logiciels libres ;
- de sensibiliser et former à l’installation et l’utilisation de ces
  services en s'inscrivant dans démarche d'éducation populaire pour
  l'utilisation des outils numériques.

## Article 4 - Siège social

Le siège social est :
chez Mr Étienne Deparis
6, allée des Maisonneuves
44140 GENESTON

# Composition

## Article 5 - Composition

L'association se compose d'adhérent⋅e⋅s, parmi lesquel⋅le⋅s sont élus
un·e Président·e et un·e Trésori·er·ère.

## Article 6 - Conditions d'adhésion

Est considéré comme membre de l'association tout⋅e adhérent⋅e à jour de
sa cotisation.

Toute personne physique ou morale peut adhérer à l'association en
s'acquittant d'une cotisation annuelle. Chaque adherent⋅e prend
l’engagement de respecter les présents statuts qui lui sont communiqués
à son entrée dans l’association.

L’association et ses membres s’interdisent toute discrimination,
veillent au respect de ce principe et garantissent la liberté de
conscience pour chacun·e de ses membres.

## Article 7 - Cotisation

La cotisation due par chaque catégorie d'adhérent⋅e⋅s est fixée
annuellement par l’assemblée générale ordinaire et apparaît
dans son compte rendu.

## Article 8 - Perte de la qualité d'adhérent⋅e

La qualité d'adhérent⋅e se perd par décès, démission, non-paiement de la
cotisation ou radiation.

# TITRE 3 - Administration

## Article 9 - Dispositions communes pour la tenue des assemblées générales

Les assemblées générales sont restreintes aux membres à jour de leur
cotisation.

La convocation doit être envoyée aux membres au minimum 7 jours inclus
avant la date retenue pour cette assemblée générale extraordinaire.

L'usage de procurations est possible. Une limite d'une procuration par
membre est imposée. Les décisions sont prises par votes. Elles sont
adoptées à la majorité absolue des votes exprimés, soit la somme des
présents et des procurations.

L'assemblée générale est compétente pour voter la modification des
statuts.

## Article 10 - Assemblée générale ordinaire

Une assemblée générale ordinaire est tenue annuellement, avec un maximum
de 404 jours entre deux assemblées générales ordinaires.

Lors de l'assemblée générale ordinaire sont présentés le rapport moral
et financier des activités de l'association depuis la dernière assemblée
générale ordinaire. Ces rapports sont soumis au vote. Un rejet de
n'importe lequel de ces rapports entraîne automatiquement la mise au
vote de la dissolution de l'association.

Un budget prévisionnel ainsi que les actions pour l'année à venir sont
également soumises au vote.

Le/la président·e et le/la trésori·er·ère sont renouvelé·e·s en
assemblée générale ordinaire. Pour ces deux postes, seules les
candidatures d'adhérent⋅es sont acceptées.

## Article 11 - Assemblée générale extraordinaire

Un tiers des membres peut convoquer une assemblée générale
extraordinaire.

Une assemblée générale extraordinaire est convoquée pour prendre des
décisions ne pouvant attendre la tenue d'une assemblée générale
ordinaire.

# Ressources de l'association

## Article 12

Les ressources de l’association comprennent :

- le montant des cotisations ;
- les subventions de l’État ou de tout autre organisme public ;
- les sommes perçues en contrepartie des prestations fournies par
  l’Association ;
- ou tout autres ressources qui ne sont pas contraire aux règles en
  vigueur.

# Dissolution de l'association

## Article 13 - Dissolution

La dissolution de l'association ne peut se faire qu'à l'occasion d'une
assemblée générale extraordinaire.

En cas de dissolution prononcée par l’assemblée générale extraordinaire,
un·e ou plusieurs liquidat·eur·rice·s sont nommé·e·s par celle-ci.

Si les assemblées générales ordinaires n'ont pas eu lieu dans le temps
imparti tel que décrit par l'article 10, n'importe quel·le membre peut
se constituer liquidat·eur·rice.

Le rôle de ces liquidat·eur·rice·s (nommés par l'assemblée générale
extraordinaire ou saisi⋅e⋅s d'eux/elles-même) est de répartir l'actif,
s’il y a lieu, à une ou plusieurs associations ayant un objet similaire
ou à tout établissement à but social ou culturel de son choix.
