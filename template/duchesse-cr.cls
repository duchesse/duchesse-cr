%!TEX TS-program = xelatex
\ProvidesClass{duchesse-cr}
\NeedsTeXFormat{LaTeX2e}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass{article}
\RequirePackage{xltxtra}
\RequirePackage{upquote}
\RequirePackage{graphicx}
\RequirePackage{float}
\floatplacement{figure}{H}

% Redefine \includegraphics so that, unless explicit options are
% given, the image width will not exceed the width or the height of the page.
% Images get their normal width if they fit onto the page, but
% are scaled down if they would overflow the margins.
\makeatletter
\def\ScaleWidthIfNeeded{%
 \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\def\ScaleHeightIfNeeded{%
  \ifdim\Gin@nat@height>0.9\textheight
    0.9\textheight
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\setkeys{Gin}{width=\ScaleWidthIfNeeded,height=\ScaleHeightIfNeeded,keepaspectratio}%

% Selection de la langue du document
% The following if you don't use xelatex
% \RequirePackage[frenchb]{babel}
% The following if you do use xelatex
\RequirePackage{polyglossia}
\setdefaultlanguage{french}
\RequirePackage[autostyle=true,french=guillemets]{csquotes}
\MakeOuterQuote{"}
\frenchspacing
\widowpenalty=300
\clubpenalty=300

\newcommand{\euro}{€}

%% Colors
\RequirePackage[usenames,dvipsnames]{xcolor}
\definecolor{primary}{HTML}{000000}
\definecolor{headings}{HTML}{000000}
\definecolor{subheadings}{HTML}{000000}
\definecolor{date}{HTML}{888888}

%% Fonts
\defaultfontfeatures{Scale=MatchLowercase}
\setmonofont[Mapping=tex-text,Scale=0.8]{Inconsolata}
\setromanfont[Mapping=tex-text]{Linux Libertine O}
\setsansfont[Mapping=tex-text]{Linux Biolinum O}

\RequirePackage{scrextend}
\changefontsizes[10pt]{10pt}

% configuration de la transformation en PDF
\RequirePackage{hyperref}
\hypersetup{
  colorlinks=true,
  urlcolor=magenta,
  pdfborder=0 0 0,
  breaklinks=true,
  bookmarksopen=true,
  pdfcreator=XeLaTeX,
  pdfproducer=XeLaTeX}

%% Margins

\RequirePackage[hmargin=2.5cm, vmargin=3cm, foot=0cm]{geometry}
\setlength{\parskip}{\baselineskip}

\providecommand{\tightlist}{
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
}

%% Headers and footers

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\lhead{}
\chead{}
\rhead{}
\lfoot{}
\cfoot{\vspace{1.5cm}\thepage}
\renewcommand\headrulewidth{0pt}
\renewcommand\footrulewidth{0pt}

%% Title
\makeatletter
\renewcommand{\maketitle}{
  \begin{minipage}[t][0.14\textheight]{\linewidth}
    \begin{flushright}
      \begin{Large}
        \textbf{\addressname} \\
        \textnormal{\addresshost} \\
        \textnormal{\addressstreet} \\
        \textnormal{\addresscity} \\
      \end{Large}
    \end{flushright}
  \end{minipage}

  \begin{minipage}[c][0.14\textheight]{\linewidth}
    \begin{center}
      \begin{huge}
        \@title \\
      \end{huge}
    \end{center}
  \end{minipage}
  \begin{minipage}[c][0.14\textheight]{\linewidth}
    \begin{center}
      \begin{LARGE}
        \textit{\@date}
      \end{LARGE}
    \end{center}
  \end{minipage}

  \begin{minipage}[t][0.14\textheight]{\linewidth}
    \begin{center}
      \includegraphics[height=12cm]{duchesse.pdf}
    \end{center}
  \end{minipage}

  \newpage
  \rfoot[B]{\vspace{0.5cm}\includegraphics{duchesse_mini.pdf}}
}
\makeatother
