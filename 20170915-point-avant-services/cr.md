# Présents

* Sacha
* Milouse
* Vinilox
* Kolbo
* Fabien
* Zeroleptik
* Romanek
* Bobby

# Ordre du jour

* Premier retour sur le mode de fonctionnement décidé lors de la
  dernière réunion de travail
* Banque et assurance
* Lieu de réunion
* Préparation café vie privée
* Point technique avant le lancement des services

# Mode de fonctionnement

Ce point n'a pas été abordé

# Banque et assurance

## Discussion

Le compte au Crédit Agricole sera ouvert courant de la semaine prochaine
(semaine 38).

Le compte est gratuit pour les chéquiers, les virements et l'accès
internet.

Un devis a été réalisé pour l'assurance, il est de 82€ au Crédit
Agricole et ne comprends pas d'aide juridique.

Il a été constaté par Vinilox et Sacha que l'association ne disposait
pas de document interne précisant les rôles de chacun, en particulier en
ce qui concerne le bureau.

Pour des raisons pratiques, Vinilox a été déclaré comme secrétaire pour
la banque.

## Actions à mener

* \#21 -- Demander un devis à la Macif pour l'assurance : Vinilox
* \#17 -- Mettre au clair le bureau de l'association
    - Contacter préfecture (Zeroleptik)
    - Contacter Thomas (Sacha)

# Finances

Finances : quelques lignes sont à rajouter dans notre trésorerie.

Des frais vont être engagés d'ici octobre.

# Lieu de réunion

## Discussion

La salle du bas chez Pioche semble convenir à tout le monde. Il faudra
la réserver en avance pour les prochaines réunions. Il est décidé que la
date de la prochaine réunion sera systématiquement choisie à l'issue de
chaque réunion, afin de réserver la salle en sortant.

## Date de la prochaine réunion

La prochaine réunion se tiendra le vendredi 6 octobre.

# Préparation Café vie privée

## Discussion générale

Duchesse interviendra au cours du
[café vie privé de Nantes du 29 et 30 septembre](https://cafevieprivee-nantes.fr/conference-gesticulee-informatique-ou-libertes-le-29-sept-a-nantes/).
Le but est de communiquer sur l'association, recruter de nouveaux
membres et en particulier des membres actifs pour seconder Sacha sur la
technique. Ce café vie privée a lieu le 30 au bar la Dérive à Nantes.

Le café vie privé commence par une conférence gesticulée, à la fin de
laquelle l'association a été cordialement invitée à se
présenter. Vinilox et Sacha s'y rendront et présenteront l'association.

Zeroleptik aborde le sujet de l'éthique des banques lors d'une
digression sur les thématiques pouvant être abordées dans les réunions
de type rencontre de l'association. Aucune décision n'est prise mais
l'assemblée s'accorde sur l'intérêt d'un tel sujet.

## Actions à mener

* \#19 -- Le site est à modifier (par Milouse) pour mettre en valeur :
    - les annonces des réunions
    - les liens vers les réseaux sociaux
* \#20 -- Vérification du hook Git de publication du site : Sacha
* \#18 -- Modification de la fiche sur le site chatons.org : Milouse
* \#13 -- Préparation du *speech* de fin de conférence gesticulée :
  Sacha et Vinilox

## Discussion sur les *flyers*

Il est rappelé le contexte dans lequel l'association avait décidé
d'éditer un flyer. Ce dernier est temporaire, ciblé sur la conférence
gesticulée et le café vie privée.

Un brouillon du flyer est distribué. Des demandes de modifications sont
exprimées.

## Actions à mener sur les *flyers*

* \#8 -- Modification du flyer : Vinilox
* \#22 -- Impression et massicot : Zeroleptik

# Point technique

## Discussion

Sacha a bossé tout le week-end du 9 et du 10 avec Greg sur les
fondations techniques. Le serveur (un dell R300) a été réinstallé avec
une Debian vierge. Les *firmwares* ont été mis à jour.

Le serveur sera réellement installé quand l'automatisation du
déploiement (à l'aide de [Saltstack](https://saltstack.com/)) sera
finalisé.

Encore un week-end de boulot avant de mettre en prod.

Il est manifesté le désir de passer par une phase de *beta test* dès que
possible. Cette phase de test est repoussée à la suite de l'installation
(*rackage*) du serveur dans le *datacenter*.

Vinilox fait part à l'assitance du fait qu'il a différents plans pour se
procurer des serveurs d'occasion supplémentaires. Leur obtention est
repoussées à plus tard, lorsque le besoin sera là.

## Actions à mener

* \#16 -- *Racker* le serveur : Sacha
