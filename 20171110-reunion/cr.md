# Retour sur la première rencontre

Une petite rencontre a eu lieu à Pioche le 21 octobre. Cet évènement
nous a servi de rôdage pour voir nos lacunes sur la communication.

Nous avons des lacunes dans notre discours et un manque de
documentation sur nos services s'est fait sentir. Des guides simples
pour leur utilisation en ligne et/ou sur papier pourraient servir de
support lors de ces rencontres.

Des fiches d’utilisation pour utilisateur final, guides «
philosophiques » pour expliquer en quoi nous sommes bien.

Quelques exemples :

- Picasoft : https://picasoft.net/co/fds2017.html
- Chaton Orléans

De plus, il nous faut orienter le discours en direction des adhésions
annuelles au lieu de se justifier sur les 6€/mois.

# Groupe de travail sur la communication

Une communciation globale, pour d’abord convaincre les utilisateurs,
avant de leur expliquer comment ça marche.

Éléments de communication :

- Site
- Réunions directes

## Documentation

Un groupe de travail lié à la communcation pour la documention.
Fabien est intéressé pour mener ce travail.


# Point comptabilité

   * Combien d'adhérent (côtisation réglée) => Au 10/11 : 11
   * Combien d'utilisateur ++ (qui payent nextcloud en plus) => Au 10/11 : 6
   * Combien qu'on donne / quand à FAImaison 
      * déjà viré 200€ pour la VM
      * décidé en réunion : **16€**	pour le serveur dans la cave. 
      * Vinilox vérifie ses coûts en électricité.
   * Comment gérer le remboursement des frais avancés par sacha, tcit,
	 Vini et Milouse ? Ce sont des dons pour sacha et Vini. Véfifier si c'est pareil pour tcit et Milouse.


   * Assurance : quels devis avons-nous ? Il faut en choisir une et y
	 souscrire. On va au Crédit Agricole parce que ça nous fait perdre moins de temps.

À noter : pour les adhésions, privilégier les virements, avec le nom /
pseudo dans le message. => pour les dons, noter « don ».

Procuration pour le trésorier à faire d’ici 4 mois (lors de la prochaine AG).

# Point technique

Du travail de stabilisation a été effectué sur la configuration du
serveur :

   * Beaucoup de corrections
   * Premières briques pour le monitoring
   * Brique LemonLdap installé par Sacha, permet de rendre éligible
	 plein de nouveaux services (notamment ceux ne supportant pas LDAP
	 pour trouver leur utilisateurs)
   * Possibilité (logicielle, manque le matériel) de gérer plusieurs
	 serveurs et de répartir les services web comme on le souhaite


## Point sur le matériel / connexion :

   * d1 va rester à la cave, pas besoin de rails
   * Il nous faut un vrai serveur principal à mettre en DC : 1U, pas
	 trop énergivore, avec un iDRAC utilisable.
   * Connexion passée chez Orange, puis Free

Clarifier les services « gratuits » (actuels et à venir à court terme).

Serveur de secours (réplique du site, sauvegarde, monitoring)
appartenant à Sacha, chez Online ; on déplace les sauvegardes et le
monitoring dès qu’on a un deuxième serveur mais il reste dispo en tant
que slave sur certains services.

Vinilox s’occupe de trouver un serveur et voit avec Sacha : 2 prises
disque, CPU atom 6000~ plein de cœurs, RAM 32 Go (concession
éventuelle à 16 Go).

SSO : mise en place de lemonldap sur auth.duchesse.chat. Il gère OAuth
et SAML pour unifier les identifiants de connexion, pour  les services
qui ne supportent pas LDAP.

Mis en place sur le forum et sur NextCloud : plus besoin de se limiter
à des outils qui supportent LDAP.

# LDH

Compte-rendu de la discussion.
Section Nantes de la LDH  veulent héberger ~30 utilisateurs, avec
NextCloud, agenda...
Adhésion : un coût important à compenser.

**Proposition à faire avant le 17 (AG).**

Prématuré d’accepter autant de monde d’un coup ? Mais super opportunité.

Pré-requis.
 - gestion des groupes

Problème de gestion des groupes sur NextCloud, solution à trouver
(LDAP ? développement à faire).

Peut être les « cercles ». Ou une instance NextCloud à part.

**Décision : instance NextCloud à part.** Donc ils sont
administrateurs tranquillement.

De plus : ce sont des utilisateurs finaux, pas nécessairement
impliqués dans l’association. Pas de souci au niveau du travail
collaboratif.

A clarifier : accès aux autres services pour eux ?

Problème éventuel d’interlocuteur ? Voir qui serait notre contact.

À noter : ils sont sur le projet depuis 3 ans, donc sur des échelles
de temps large.

# Quadrapero

Point sur le mail de Caps :

Est-ce qu'on tente de le co-organiser, de le prendre comme prochaine rencontre ?
Le mail a été diffusé, les intiatives individuelles sont les bienvenues.

À faire : Répondre qu’on diffuse à nos membres, collab’ avec FAImaison.

# Créer un dynamisme hors-réunions

Problèmes d'outils ?
ex : le groupe de travail sur les flyers a dérivé dans le chat

Un nouveau forum : discussions.duchesse.chat ? et/ou relancer la ML ?
Forum : on est d’accord, ca marche bien mieux pour certains sujets
Beaucoup mieux pour l’image publique, GitLab fait peur aux gens non techniques.
Il faudrait migrer les tickets actuellement sur matou dans une
catégorie sur le forum (possibilité de gérer des sous-catégories, et
gérer des droits utilisateurs).

Complémentaire au chat : **penser à faire des résumés si discussions
intéressantes**.

1 tag interne non accessible, le reste en public.

Nouveau MUC : matou@duchesse.chat - déjà en activité (pour ne pas
faire peur aux gens avec les discussions techniques). Éviter de parler
de la cave en public, ça fait peur aux gens.

## Gestion des tickets :

Doit-on nommer une personne pour s'assurer de l'avancement des tickets
entre deux réunions ? Si oui qui ?

Fonctionner davantage en projets, avec un responsable qui s’occupe des
tickets associés.

Besoin d’un « vrai » outil de suivi de projets ? On ne peut pas rester
sur GitLab.

Possibilité de déléguer la gestion des comptes : si quelqu'un veut
s'en charger.

# Les choses à faire d'ici à la prochaine réunion

   * Groupe de travail _site internet / communication_ (personnes
	 moteurs : Etienne, ealhad, zeroleptic)
   * Groupe de travail _gestion des comptes utilisateurs_ (personnes
	 moteurs : sacha, fabien, kolbo)
