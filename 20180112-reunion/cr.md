# Présents

- Milouse
- Fabien
- Kolbo
- Ehalad
- Romanek
- Bobby
- Matthieu


# Point communication

## CR première réunion equipe com

Une petite équipe de volontaires (Zéroleptik, Bobby, Romanek et Milouse)
se sont réunis le 3 janvier pour clarifier le rôle et les moyens d'une
équipe communication pour l'association.

Ils se sont mis d'accord sur le fait que l'équipe devait être autonome
sur ses décisions (pas besoin référer au bureau lorsqu'elle souhaite publier
quelque chose). L'équipe communication peut se saisir elle même de tout
sujet relatif à la visibilité de l'association. L'association peut à
tout moment soliciter l'équipe communication si elle souhaite réaliser
quelque chose. Il est important de noter que l'équipe communication a
pour rôle la communication extérieure de l'association. Ce n'est pas
l'équipe de support interne.

L'autonomie de l'équipe est garantie par le mandat que donne
l'association, au cours d'une réunion ordinaire, à tout membre
souhaitant intégrer l'équipe communication. Cette nomination est
transcrite dans le compte-rendu de la réunion. La responsabilité de ses
membres et ce qu'ils publient est confiée à l'équipe communication qui
s'organise comme elle le souhaite.

Seule l'équipe communication a les accès au site vitrine et aux
différents comptes sociaux. Concernant ces comptes sociaux, il a été
décidé par l'équipe communication l'arrêt de l'utilisation de Diaspora
au seul profit de Twitter et Mastodon pour l'instant. Les médias sociaux
peuvent être utilisés pour relayer des nouvelles d'autres associations

Lors de cette première réunion de l'équipe communication, un travail de
fond a été effectué sur le positionnement thématique de l'association
(afin d'aider les futures prises de paroles, publications,
affiches…). Cette réflexion s'est menée autour
d'[une carte mentale](https://framindmap.org/c/maps/438589/edit)

L'équipe utilise comme outil de travail la mailing list
com@listes.duchesse.chat. Cette liste n'est accessible qu'aux membres de
l'équipe. De ce fait, pour contacter l'équipe, le voix la plus simple
reste une discussion publique sur adherents@listes.duchesse.chat ou sur
le canal xmpp.

Pour finir, voici les tâches en cours :

- Début de création d'une charte graphique complète
  + réflexions sur le logo, les couleurs, les polices… (Romanek et Bobby
    en support)
  + revoir la feuille de style du site (Bobby)
- Installation d'un wordpress sur le domaine principal pour faciliter le
  travail de l'équipe et l'intégration de non-informaticiens.

## Quadrapéro du mercredi 31/01

Suite au quadrapéro de décembre, l'idée de coordonner les associations
Nantaises du domaine a émergée. Un appel aux associations Nantaises a
été fait pour faire une réunion où elles se présenteraient et prendraient
contact. Le but étant de faire naître des synergies, évènements
conjoints etc.

Cette réunion sera le 31/10 à Pioche. Milouse s'y ait inscrit pour
représenter Duchesse.

Un appel aux volontaires pour l'accompagner est fait.

## Forum Écopôle du jeudi 22/02

Pour répondre au besoin de culture commune à l'échelle du réseau,
Écopôle (CPIE Pays de Nantes qui est le réseau de l'environnement de
l'agglomération nantaise) organise chaque année un forum de
l'environnement. En 2018, la thématique retenue est « vert le
numérique. Un numérique écologique ?  Parlons-en. ». Il aura lieu le 22
février, de 14h à 22h30 à la salle Nantes-Nord en présence de Philippe
Bertrand, Journaliste et animateur de « Carnets de campagne » sur France
Inter.

Plus particulièrement, ils sont à la recherche d'un intervenant qui
apporterait son éclairage pour l'atelier 1 : Quels impacts écologiques
de notre consommation de numérique ?

L'idée est d'échanger sur l'atténuation des impacts écologiques du
numérique et d'identifier quelques solutions à mettre en œuvre

Cet atelier commence à 15H30, pour finir à 18h15. Il s'articule autour
de 2 fils conducteurs :

partie a. : Peut-on rêver d'équipements numériques sobres ?

- Introduction sur les usages : de quels usages parle t-on ? Quels
  usages souhaite-t-on ?
- équipements et sobriété
- prolonger les usages ?
- nouveaux arrivés : le smartphone et les objets connectés
- Le recyclage et les métaux rares

partie b. Vers des pratiques numériques et des logiciels vertueux ?

- introduction sur les usages visés

Milouse a donné son accord de principe pour que Duchesse y
participe. Des volontaires sont demandées pour co-construire les
réflexions qui pourront être apportées aux forum.


# Point Financier

Actuellement les entrés et sorties d'argent sont à l'équilibre. Il reste
des inconnus dont l'assurance et le paiement de la connexion a FAImaison
(que ce soit en cave ou en DC).

La discussion s'engage sur la révision du coût de l'adhésion et des
services. Pour rappel :

- 20€/an adhésion ;
- 6€/mois min pour services.

Il est dit l'importance de mettre en place un outil de transparence
financière : dire sur site tout les mois combien d'argent entre dans les
caisses et combien en sort.

Il est noté que la limitation à 50Mo actuelle est ridicule. Il
semblerait plus pertinent de soit complètement interdire l'accès au
stockage de fichier, soit augmenter cet espace pour quelque chose
d'utilisable, soit 1 ou 2Go.

Une idée est également apportée de remplacer le coût mensuel d'accès aux
services par un deuxième type d'adhésion. On aurait une adhésion simple
avec tous les services et 1, 2 Go voire 5Go d'espace de stockage pour
20€ par an et une adhésion plus haute avec un espace « illimité ».

Un point d'attention est signalé : est-ce que les utilisateurs de
l'adhésion « premium » ne s'attendront pas à avoir un support
« premium » également, ce qui n'est pas souhaitable.

Du coup le consensus s'oriente vers une adhésion simple à 20€ et, si
adhésion premium il y a, cette dernière ne donnera pas forcément accès à
plus d'espace, mais sera « vendue » comme étant une adhésion de
soutien.

Il sera noté en parallèle que si un membre souhaite utiliser plus
d'espace, il pourra lui être accordé sur simple demande de sa
part. L'idée étant de responsabiliser les usages et potentiellement
inciter un membre utilisant beaucoup d'espace à faire un don.

Votes, sur 6 membres présents :

- pour passer à 2 ou 5Go avec juste l'adhésion : 5 votes pour 1 contre
- suppression des 6€/mois pour les services : 6 votes pour
- possibilité de mettre en place campagne de dons : 6 votes pour

Une adhésion simple à 20€ n'est pas suffisante pour financer
convenablement l'association. Il convient donc d'inciter les membres à
donner un peu plus. L'association détermines trois possibilités de
communication à ce niveau :

- promouvoir une adhésion à 20€ et inciter les utilisateurs à faire des
  dons non affectés en plus pour soutenir l'association (1 personne est
  favorable à cette idée sur les 6 présentes)
- promouvoir une adhésion à 20€ et mettre en avant le fait que les
  services s'utilisent à prix libres et demander aux membres de financer
  les services utilisés en payant un montant libre (4 personnes
  favorables sur les 6 présentes)
- promouvoir différents niveaux d'adhésions (aucune personne n'est
  finalement favorable à cette option).

# Point technique

## Services installés

Openproject a été [installé](https://projets.duchesse.chat/). Il permet
le suivi des travaux en cours, mais également le suivi de réunion.

Il est vérifié que tous les membres présents ont bien accès à Openproject.

## Nouveaux services à installer

- un wordpress pour le site (demande de l'équipe com, validé par
  Sacha) ;
- un framadate.

# Prochaines réunions

## Réunion technique

Demandée par Sacha afin de commencer à former d'autres membres aux
outils d'administration technique de l'association. Sont intéressés
Fabien, Kolbo, Milouse et Romanek.

[Un sondage](https://umaneti.net/poll/studs.php?sondage=gq4x9yqnv5gsckqa)
pour trouver la date est lancé.

## Prochaine rencontre

- Ne pourra pas se faire le 31/01 pour ne pas empiéter sur le Quadrapéro.
- Plutôt une grosse réunion pour pouvoir faire des démonstration, aider
  les nouveaux membres à installer les logiciels nécessaires sur leurs
  machines…
- Sont intéressés Milouse, Bobby, Fabien.

[Un sondage](https://umaneti.net/poll/studs.php?sondage=ugqedw6qko5b93dd)
pour trouver la date est lancé.

## Prochaine réunion

[Un sondage](https://umaneti.net/poll/studs.php?sondage=qferngby1icg8f2s)
pour trouver la date est également lancé.
