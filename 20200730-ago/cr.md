# Présent·es

- Milouse
- Kolbo5

# Déroulé de l'Assemblée Générale

L'Assemblée Générale Ordinaire (AGO) débute à 17h00.

Milouse a procédé au rappel de l'ordre du jour, comme suit :

- Ouverture de l'Assemblée
- Bilan moral
- Bilan financier
- Renouvellement du bureau

## Ouverture de l'Assemblée

Milouse rappelle que cette présente Assemblée a été convoquée
régulièrement comme l’association est tenue d’en tenir une
annuellement.

## Rapport moral de l'association

Le rapport moral de cette année couvre toute la période de mai 2019 à
juin 2020.

Aucune activité n’a été entreprise par aucun des membres durant cette
période, tant d'un point de vue que technique que d'un point de vue vie
associative : en dehors des deux dernières assemblées, les membres ne se
sont pas rencontrés.

Milouse a coupé durant l'été le serveur qui était resté dans le
*datacenter* de FAIMaison, faute d'usage et à la demande de cette
association qui avait besoin de récupérer de la place dans la baie.

### Vote

Le rapport moral est soumis au vote.

Sur les deux personnes présentes :

- 2 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Bilan financier

Kolbo5 commence la présentation du rapport financier. Sa présentation
utilise le tableau visible dans la [figure 1][financial results] comme
support.

![Résultat financier sous forme de tableau][financial results]

[financial results]: ./tresorerie/resultat_duchesse.png

Il est à noter qu'en plus de ne favoriser aucune activité entre membre,
les finances de l'association ne se portent pas bien. Malgré le passage
chez Hetzner pour une plus petite machine, le fait d'avoir conservé « au
cas où », un serveur chez FAIMaison a coûté très cher pour le peu de
membres de l'association, alors même que ce serveur n'a finalement pas
servi et que Duchesse n'a jamais réussi à être en mesure de participer
de manière correcte à la location de la place en baie qu'elle utilisait.

### Vote

Le bilan financier est soumis au vote.

Sur les deux personnes présentes :

- 2 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Renouvellement du bureau

La composition du bureau existant est rappelée, d'après la déclaration
en préfecture. Les personnes suivantes en faisaient partie :

- Milouse
- Kolbo

Candidat·es pour le nouveau bureau :

- Présidence : Étienne Deparis (dit Milouse)
- Trésorerie : Emmanuel Olivier (dit Kolbo5)

### Vote pour l'élection d'Étienne Deparis au poste de président

Sur les trois personnes présentes :

- 2 votes pour
- 0 vote contre
- 0 votes blancs ou abstentions

### Vote pour l'élection d'Emmanuel Olivier au poste de trésorier

Sur les trois personnes présentes :

- 2 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Modification des statuts

Les statuts ont été modifiés pendant cette AGO pour prendre en compte la
nouvelle adresse du président. La nouvelle adresse de l’association est :

chez Mr Étienne Deparis
6, allée des Maisonneuves
44140 GENESTON

### Vote pour la modification des statuts

Sur les trois personnes présentes :

- 2 votes pour
- 0 vote contre
- 0 vote blanc ou abstention


# Clôture de l'Assemblée

L'Assemblée Générale Ordinaire se termine à 17h50.
