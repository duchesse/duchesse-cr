Compte rendu de la réunion du 15/06/2017

# Ordre du jour (Avant la réunion)

 1. Accueil des nouveaux (si besoin)
 2. Retour sur ce qui a été fait depuis la dernière réunion
 - mise en place XMPP
 - recherche et obtention de serveur(s)
 - budget

# Points abordés :

## Communication

 Au vu de la difficulté à organiser cette réunion, le besoin
 d'organiser cela est apparu nécéssaire.

   * Il a été décidé que la prochaine réunion commencera un cycle de
	 réunion mensuel, si possible à lieu fixe. L'inscription à la
	 mairie de Nantes vient d'être faite pour obtenir au besoin une
	 salle de réunion de taille adaptée.

   * Il faut  tenir compte des contraintes et notamment :
	 * FAImaison se réunit  le jeudi et beaucoup de menbres ne sont pas disponible ce jour là.
	 *  Il y a besoin d'un secrétaire pour les réunions / CR.
	 * Organisation de la vie de l'association à mener avec plus de personnes.

## Point compta

 - Qu'est ce qui a été avancé, quels sont les devis, les projections, les envies d'achat… → feuille calc Vini
 - Compte bancaire
 - Rentrées d'argent
 - Définir montant adhésion

 Les points qui ressortent et nécessiteront d'être discutés/confirmés en mailing-list:

   * l'adhésion est proposé à un prix de 20 €
   * prix fixe pour les services : 6€/mois à débattre *rapidement* car
	 le prix va dépendre du nombre d'abonnés pour assurer l'équilibre
	 financier.
   * besoin d'un tapis de 6 mois : appel à donations sur la ML somme à
	 atteindre 500€. Permet de finaliser le serveur, payer banque,
	 assurance et les premier frais.

Le lancement des services est prévu en septembre.

Le travail de cet été va être de faire rapidement des demande auprès
des banques pour l'ouverture d'un compte bancaire (vini)  ainsi que
divers assurances (fabien+vini).

Une fois tous cela crée, Duchesse pourra accepter les adhésions.

## Technique

- ldap
- nextcloud
- coin
- xmpp

### Résumé depuis la précédente réunion :

- Rôle ansible (outil automatique de déploiement de serveur) fait par
  sacha
- Les rôles sont disponibles sur framagit (Besoin de formation avant
  que n'importe qui puisse prendre la main)

- LDAP installé (annuaire utilisateurs)
- Backend pour les futurs services
- Test d'installation Nextcloud. Recette en cours d'écriture.
- Coin (gestion d'association). Besoin de vérification.
  - Ajouter modules services.
  - Réflexion autour du module LDAP.
- XMPP est quasi prêt. Fourni en service de base à terme. Possibilité
  de fournir des comptes. Potentiellement plusieurs personnes
  interressées.
  - Possibilité de fournir une adresse avec son DNS personel.

- Nextcloud mise en place du discovery pour faciliter l'utilisation
  dans des compagnons mobiles.

Reste à faire :

- backups
- movim ?
- mails internes (possibilité technique)

- Comptes XMPP : possibilité d'en fournir la semaine prochaine,
  communication à faire la ML.

### Serveurs

L'association à reçu 3 dons de machine
  -  1 serveur très energivore éliminé comme serveur principal. Il
	 sera peut-être une solution de secours ou de backup dans le
	 futur, mais reste sur l'étagère pour l'instant.
 - 1 serveur type R300 consommant (75W à 100W) qui a été choisi pour
   fournir les premiers services.
 - le 3ème serveur à des caractéristiques intéressante pour un
   routeur.

## Fin de réunion

Il y a eu un dernier rappel sur le risque sur passer les services sur le prix de l'adhésion de l'asso.
La communication publique repoussée vers le mois de septembre.
Un grand merci à Caps pour la solution de repli trouvé quelques heures avant le début de réunion.
