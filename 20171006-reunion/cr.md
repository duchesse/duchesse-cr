# Présents

* Sacha
* Milouse
* Vinilox
* Kolbo
* Fabien
* Zeroleptik
* Romanek
* Bobby

# Ordre du jour

* Point comptabilité et assurance
* Retour sur la mise en rack du serveur
* Retour sur la présentation de Duchesse à la conférence de Lunar et au
  café vie privée (CVP) ayant suivi
* Point technique
* Discuter des applications pouvant être installées


# Mot d'accueil

Nous sommes officiellement lancés. Merci à tout le monde pour le travail
effectué au cours du mois de septembre.


# Point comptabilité et assurance

Le compte bancaire est ouvert. Le RIB est disponible, nous pouvons
prendre les adhésions. Quelques membres présents donnent des chèques
d'adhésion ou confirment la mise en place de virement.

Côté assurance, la MACIF est mise en *stand-by*. Il est plutôt proposer
de demander au Crédit Agricole les conditions de l'assistance
juridique.

# Retour sur l'opération *Rackage*

Échec de la mission, les rails fournis avec le seveur étant trop
courts. Par ailleurs, aucune solution artisanale n'est apparue jouable
dans le *datacenter*.

Du coup, le serveur est placé jusqu'à nouvel ordre chez Vinilox derrière
un VPN FAImaison. Pour l'instant dans son salon, il sera descendu dans
la cave dans le week-end.

# Retour sur la conférence de Lunar et le Café Vie Privée (CVP)

La présentation de Duchesse à la conférence de Lunar a été faite par
Sacha et Vinilox. Il y a eu peu de retour directement suite à la
conférence (dans le petit temps d'échange sur le parvis de la salle).

Les retours ont été plus nombreux le samedi à la suite d'un petit
créneau prévu -- à l'invitation des organisateurs du CVP --
pour présenter à nouveau l'association. Milouse s'y est collé cette
fois-ci.

Il a été discuté avec les organisateurs du CVP de la possibilité
d'organiser un « atelier Duchesse » sur un prochain CVP. L'idée a été
notée comme étant faisable\footnote{A noter que depuis, un échange de
mail a montré qu'il est absolument nécessaire de prévoir un atelier
neutre, au cours duquel seulement Duchesse serait évoqué. Bref, c'est à
rediscuter (Note de l'éditeur)}.

Vinilox nous fait un retour médiocre du CVP du lundi suivant, lié à la
présence d'une population trop technique.

À noter que sur l'ensemble des retours obtenus, la notion de prix est
revenue plusieurs fois. Les six euros par mois peuvent faire peur sans
dialogue. Il va falloir communiquer plus efficacement sur le prix des
services.

# Point technique

La parole est donnée à Sacha.

Côté technique, il y a eu un gros besoin de faire une architecture qui
va héberger un grand nombre de services. On a vu que pour cibler un
grand nombre d'utilisateurs, on va devoir ratisser large sur les
besoins. Donc des stacks techniques et langages différents sur chaque
logiciels. Sacha voulait se fermer le moins de portes techniques
possibles; il a donc travaillé pendant 3 semaines pour pouvoir gérer
plusieurs domaines, certificats et différents services.

On a donc actuellement un serveur chez Vinilox, accessible au travers
d'une connexion FAIMaison, tournant sous Debian avec plein de conteneurs
LXC hébergeant différents services d'infrastructure (pgsql, ldap,
backups, reverse proxy, …). Le but est d'être le plus élastique possible
afin de pouvoir facilement répondre à toute demande future d'un service
en particulier et ainsi de continuer sur notre lancée.

Encore quelques trucs urgents à finir, Sacha a fini les backups ce jour.

La gestion des logs pour l'aspect légal est également à traiter.

Il reste quelques mois de peaufinage, mais c'est déjà beaucoup plus cool
en terme de charge de travail.

Il en est à un stade où le lancement d'un nouveau service se fait dans
la semaine tranquillement. D'ici deux à trois semaines, la transmission
des connaissances sera beaucoup plus simples et pourra débuter. Une
documentation complète est en cours de rédaction.


# Quelles applications installer

Pour rappel, voici la liste des applications actuellement instalées sur
Nextcloud :

- tâches
- notes
- calendrier
- contacts
- fichiers

Après un tour de table des participants et un sondage à main levée, il
est décidé d'ajouter les applications suivantes :

- TOTP
- Quota warning

Le tour de table a permis également de lister les applications -- pas
forcément liées à Nextcloud -- qui seront intéressante à
installer. Elles sont listées par ordre d'arrivée probales :

- pads (novembre) ;
- framadate / opensondage (novembre) ;
- stockage de mots de passe en ligne -- pour rejoindre les questions
  d'hygiène numérique vues en CVP et parce que c'est un besoin réel pour
  le fonctionnement de l'infrastructure ;
- lecteur RSS -- le tour de table a permis de déterminer qu'il était
  certainement préférable d'utiliser une solution externe à Nextcloud,
  plus complète, plus fiable. Cette application reste donc à déterminer ;
- collabora online -- un certain nombre des présents ont manifesté leur
  intérêt pour cette application, plutôt lourde, qui demandera
  obligatoirement un serveur dédié. Cependant sa plus-value pour un
  public associatif a été noté et plaide largement en sa faveur ;
- mails -- les retours obtenus durant les différents CVP on mis en
  exergue le besoin, finalement, de proposer de l'hébergement de
  mail. Il n'est pas encore décidé à ce jour d'officiellement le faire,
  mais la question est notée comme étant plus que jamais sur la
  table. D'autant que l'hébergement de mail est déjà techniquement une
  réalité d'un point de vue technique -- pour les adresses miaou@ et
  matou@ par exemple ;
- wallabag ;
- syncro firefox.
