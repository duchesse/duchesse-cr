# Présent·es

- Milouse
- Kolbo5
- Sacha
- Zeroleptik
- Fabien
- Robin
- Gordon
- Tcit (à distance)
- Vinilox (arrivé à après le vote du rapport financier mais a participé à ce vote)

# Déroulé de l'Assemblée Générale

L'Assemblée Générale Ordinaire (AGO) a commencé à 20h08.

Milouse a procédé au rappel de l'ordre du jour, comme suit :

- Bilan moral
- Bilan financier
- Annonce du budget prévisionnel
- Fixation du montant de la cotisation
- Renouvellement du bureau
- Modification des statuts
- Séance de questions et réponses avec les adhérent·es

## Rapport moral de l'association

Le rapport moral de cette année couvre toute la période de la création
de l'association à la présente assemblée générale.

L'association a été officiellement créée (publication au journal
officiel) le 18 mars 2017. Suite à cette publication, le collectif a
subi une baisse d'activité assez conséquente, conduisant d'ailleurs au
départ de quelques membres historiques.

Sous l'impulsion de Sacha et Vinilox, les travaux ont été relancés à
partir de mi-août. Un serveur de récupération a été installé et
configuré pour accueillir Nextcloud. Étant dans l'impossibilité dans
l'immédiat d'installer le serveur dans la baie de FAImaison, il a été
décidé de temporairement faire de l'hébergement chez un membre
FAImaison.

Le lancement des premiers services orientés autour de Nextcloud a eu
lieu début septembre.

Le choix de financement de ces premiers services et le mode d'adhésion
pour les membres ont été les suivants :

- d'une part l'adhésion, qui permet aux membres de s'impliquer dans
  l'association — être invité aux AGs, participer aux réunions
  décisionnelles, … — et de bénéficier de l'accès aux services les moins
  gourmands en ressources. L'adhésion a été fixée à 20€ par an ;
- d'autre part un abonnement à payer à part pour les services plus
  intensifs — pour l'instant Nextcloud seul —, sous la forme d'un prix
  libre, avec un minimum de 6€ par mois.

Cette dynamique de lancement des services et l'ouverture des adhésion a
permis la constitution d'un noyau dur de membres, constitués *de facto*
en Matou, selon nos statuts. Notons en particulier le travail de suivi
sur la comptabilité effectué par Kolbo5 depuis. Le compte en banque de
l'association a été ouvert le 6 octobre.

Octobre a été l'occasion d'ajouter d'autres services de fond et en
particulier les listes de diffusions, dont certaines ont remplacé les
boîtes de courriers électroniques institutionnelles.

L'automne a également été l'occasion de mettre en œuvre le cycle de
fonctionnement de l'association avec deux rendez-vous par mois, à
savoir :

- une « rencontre », dédiée à l'accueil des nouveaux, à l'échange entre
  membres, à la formation sur l'utilisation de nos outils ;
- une « réunion », dédiée au strict suivi associatif et administratif :
  vérification des comptes, décision sur les services à ouvrir,
  répartition des tâches à faire…

Malgré les bonnes volontés, les rencontres sont restées peu ouvertes sur
l'extérieur et quasi aucun nouveau membre n'est entré dans l'association
par ce biais. Depuis décembre, ce format de rendez-vous est
factuellement abandonné. Seules les « réunions » subsistent et ont été
tenues régulièrement.

Au début de l'hiver l'association a été invitée plusieurs fois à se
présenter lors de différents évènements : conférence gesticulée de
Lunar et sur un Café Vie Privée nantais.

En décembre également, l'association a pu faire parler d'elle au travers
d'une intervention de Milouse dans une émission de radio diffusée sur
Prun'.

Autour de Noël, des réflexions ont été menées par certains membres sur la
refonte graphique et la communication au sens large de l'association,
que ce soit en interne entre adhérents, mais également à destination du
grand public. Différentes productions ont été réalisées à cette
occasion.

Janvier a été l'occasion pour les membres actifs de se pencher
sérieusement sur la question du financement de l'association, à la
lumière des quatre mois écoulés depuis l'ouverture des adhésions. Il est
apparu que les formules de financement évoquées précédemment étaient
compliquées à expliquer aux potentiel·les futur·es adhérent·es et
pouvaient même desservir l'association. Il a donc été convenu de les
abandonner pour les remplacer par une formule plus simple sous la forme
d'une adhésion de 20€, à laquelle s'associe une participation libre au
financement des services.

Un nouveau serveur plus récent, économique et écologique a été acquis
en début d'année afin de pouvoir disposer d'une base technique solide
pour les prochaines années. Des dépenses ont été engagées dans ce sens
pour l'acquisition d'un SSD et de barrettes de mémoire. Le serveur a
été rapidement installé, configuré et racké dans la baie de FAImaison
pour remplacer l'ancien serveur toujours hébergé chez un adhérent. De
nouveaux services pourront ainsi être aisément installés dans les mois
suivants.

Le 22 février, Milouse a participé au nom de Duchesse au forum du réseau
de l'environnement organisé par Écopôle à Nantes. Ce rassemblement avait
pour but de faire se rencontrer différents acteurs et actrices de la
métropole nantaise, tant association qu'entreprises ou pouvoirs publics,
pour s'interroger sur les impacts des activités numériques sur
l'environnement.

Depuis, une baisse d'activité s'est faite à nouveau ressentir. La
dernière réunion de l'association s'est rappelé qu'il était nécessaire
d'organiser l'assemblée générale avant le 8 avril : c'est chose faite.

### Vote

Le rapport moral est soumis au vote.

Sur les 8 personnes présentes :

- 8 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Bilan financier

À 20h29, Kolbo5 commence la présentation du rapport financier. Sa
présentation utilise l'image ci-après comme support.

![Tableau présentant les résultats et bilans de l'association](./resultat_duchesse.png)

Le rapport est fait sur l'année calendaire (jusqu'au 31 décembre 2017).

Sur les trois premiers mois de 2018 il est à noter que l'association a
pris possession de son nouveau serveur. Cela fait monter les actifs
matériels à 419€, dont 287,95 € de barettes de RAM et de disques SSD.

Aujourd'hui l'association a 63 € sur le compte, sachant qu'il faut
encore rembourser 60€ à Tcit et Vinilox. Le paiement régulier d'accès
aux services devrait permettre le renflouement des caisses.

L'association compte aujourd'hui sept adhérents qui paient mensuellement
pour l'accès aux services, sur un total de treize adhérents à jour de
cotisation.

Il est également à noter que l'association n'a pour l'instant souscrit à
aucune assurance.

### Vote

À 20h39, le bilan financier est soumis au vote.

Sur les neuf personnes présentes (Vinilox a rejoint le groupe au cours
de l'exposé) :

- 9 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

### Budget prévisionnel

- L'association paie 16 € par an d'adhésion à FAIMaison.
- L'association paie 16 € par mois la location d'un espace 1U à
  FAIMaison (soit 192 € par an).
- La souscription à une assurance devrait ajouter à peu près 90 € par
  an de charge.

Sacha demande si quelqu'un se souvient du résulat du sondage sur le prix
à donner à FAImaison pour l'hébergement. Personne ne peut répondre à
cette question.

En entrée, en omettant les adhésions, donc en calculant uniquement sur
la base de sept personnes donnant 6 € mois pour l'accès au service, soit
504 € par an, le bénéfice sur l'année serait de 296€. Cela devrait
permettre d'augmenter la participation libre pour l'hébergement du
serveur auprès de FAIMaison. Avec un paiement de 30 € par mois,
l'association resterait bénéficiaire.

## Montant de la cotisation 2018

Le montant de l'adhésion avait été fixée officieusement à 20 € par an
lors depuis la reprise en septembre. Il s'agit d'acter officiellement ce
montant, comme le stipulent les statuts.

### Vote

À 20h47, la fixation de la cotisation annuelle à 20 € est soumise au
vote.

Sur les neuf personnes présentes :

- 9 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

## Renouvellement du bureau

La composition du bureau existant est rappelée, d'après la déclaration
en préfecture. Les personnes suivantes en faisaient partie :

- Sacha
- Tcit
- Vinilox
- Zeroleptik

Les postes n'avaient pas été clairement définis en interne, ce qui
résultait en sentiment de malaise vis-à-vis des démarches officielles à
effectuer.

Pour éviter cette situation de flou, Milouse propose d'élire un·e
président·e et un·e trésorier·e qui seront clairement établi·es et
déclaré·es comme tel·les à la préfecture. La notion de Matou sera
conservée, mais pour un usage strictement interne. Le ou la présidente
n'aura aucun droit factuel sur l'association, le fonctionnement est
appelé à rester collégial, dans l'esprit de l'association et tel que
déclaré dans les statuts.

Vinilox demande si le vote porte également sur l'élection d'un·e
secrétaire. Après discussion, il est décidé de se cantonner à un bureau
limité à deux personnes avec un minimum de responsabilités.

Candidat·es pour le nouveau bureau :

- Présidence : Étienne Deparis (dit Milouse)
- Trésorerie : Emmanuel Olivier (dit Kolbo5)

### Vote pour l'élection d'Étienne Deparis au poste de président

Sur les neuf personnes présentes :

- 7 votes pour
- 0 vote contre
- 2 votes blancs ou abstentions

### Vote pour l'élection d'Emmanuel Olivier au poste de trésorier

Sur les neuf personnes présentes :

- 8 votes pour
- 0 vote contre
- 1 vote blanc ou abstention

Tcit part à l'issue de ce vote et donne procuration à Gordon pour la
suite de l'AGO.

## Modification des statuts

À 21h13 après une petite coupure, la discussion reprend sur la question
de modification des statuts. Il est rappelé à cet effet que l'Assemblée
Générale Ordinaire est compétente sur la modification des statuts de
l'association.

Milouse propose, au vu du long mail reçu sur la liste des adhérents de
l'association – contenant beaucoup de propositions très intéressantes –, mais
dont l’auteur n'était pas présent ce soir là et enfin en l'absence de
proposition concrète, de soumettre au vote la remise à plus tard les
modifications des statuts. Il propose plus spécifiquement de voter le fait que
des gens se mettent à travailler sur des modifications et que l'on convoque une
Assemblée Générale Extraordinaire (AGE) pour voter ces modifications lorsque le
groupe de travail évoqué les considèrerait suffisamment avancées. La présente
AGO n'aurait plus qu'à nommer une personne référente pour encadrer ce groupe de
travail.

Si cette proposition est votée, Milouse se propose comme référent pour
ce chantier.

Kolbo5 propose de fixer une date butoir pour la tenue de cette AGE,
pour motiver les troupes et s'assurer que le travail soit fait au plus
vite quand même.

Milouse propose de voter pour une tenue de cette AGE avant le 30 mai.

### Vote

Concernant la tenue d'une AGE avant le 30 mai, afin de voter un certain
nombre de modifications des statuts, telles que proposées par un groupe
de travail qui se réunira d'ici cette date ; sur les huit personnes
présentes et une procuration :

- 7 votes pour
- 0 vote contre
- 2 votes blancs ou abstentions

Concernant l'inclusion des modifications à soumettre au vote dans la
convocation à l'AGE qui sera envoyée aux adhérents ; sur les huit
personnes présentes et une procuration :

- 9 votes pour
- 0 vote contre
- 0 vote blanc ou abstention

Vinilox quitte l'AGO à l'issue de ce vote.

## Questions diverses

À l'issue des votes prévus à l'ordre du jour, l'Assemblée Générale
Ordinaire entame le dernier point prévu, à savoir une séance de
questions libres à l'adresse du Matou de la part des adhérent·es qui le
souhaitent. Il s'agit également d'un forum assez libre ou chacun·e est
libre de s'exprimer.

Sacha prend la parole pour exprimer son avis sur l'année écoulée et
faire part à l'assemblée de ses craintes et avis sur
l'avenir. L'association a peu de membres actifs. Les membres se sont
accordés en interne sur le fait qu'il fallait malheureusement faire avec
pour faire avancer l'association au mieux en évitant de se mettre la
pression – il est rappelé que cela a pu être le cas par le passé, vécu
comme un problème. Il reste des tâches de transfert de compétences
techniques – actuellement, le savoir technique est porté exclusivement
par Sacha. Il reste des services à mettre en place – dans la lignée des
petits outils Framapad, Framagenda… –, il serait souhaitable de les
lancer dans la première partie d'année.

L'association a des difficultés à faire les choses en temps et en heure,
notamment concernant la communication extérieure, qui a très rapidement
été un gros point faible. L'association a eu un peu de visibilité grâce
à des évènements extérieurs, mais a d'énormes progrès à faire
là-dessus. L'association a également du travail à faire sur
l'inclusivité – trop grande barrière technique dans la posture des
membres, la tenue des réunion –, plusieurs personnes ont été perdues à
cause de cela. Il est nécessaire de réussir à parler de nous en dehors du
cadre technique.

Zeroleptik aimerait augmenter le nombre d'adhérents, en dehors du noyau
technique actuel. Il rappelle qu'il existe des méthodologies pour
cela. Il propose de se rapprocher d'autres associations et en
particulier du milieu de l'économie sociale et solidaire (ESS). Un
travail de communication est en cours avec Románek sur des *flyers*.

Zeroleptik propose de travailler sur deux axes forts :

- pour intéresser « monsieur et madame tout le monde ».
- pour intéresser les associations – cette proposition est perçue comme
  un élément fédérateur. Il partage sa sensation qu'il manque une
  solution d'édition collaborative de fichiers – du type de
  [LibreOffice Online](https://wiki.documentfoundation.org/ReleaseNotes/5.3/fr#LibreOffice_Online).
  Il s'agirait pour lui d'héberger des services et de concevoir des
  offres plus adaptées aux associations, par rapport aux services et
  offres actuels plutôt ciblés sur des individus.

L'enjeu de cette dernière direction est de réussir à attirer des
associations en justifiant le coût des services – les associations sont
susceptibles de ne s'intéresser à Duchesse que si le prix demandé est
moindre ou équivalent aux offres du marché, proposées par les GAFAM. Il
est rappelé qu'il peut être important de réaffirmer le fait que
Duchesse ne fait pas des outils, mais des services, avec une valeur
ajoutée humaine très importante.

Gordon rappelle qu'il est important d'inclure les membres – y compris
les associations – dans le fonctionnement technique de l'association,
plutôt que de s'attendre à ce qu'iels se comportent en « clients ».

L'Assemblée Générale Ordinaire se termine à 21h43.
