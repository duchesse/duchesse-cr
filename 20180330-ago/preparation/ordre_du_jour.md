Ordre du jour de l'Assemblée Générale Ordinaire
===============================================

20h08 :: Début de l'AG

- bilan des activités, rapport moral (DÉCISION) ;

- bilan financier – rapport, comptes de l'exercice et affectation du
  résultat (DÉCISION) ;

- annonce du budget prévisionnel ;

- fixation du montant de la cotisation (DÉCISION) ;

- renouvellement du bureau (DÉCISION) ;

- modification des statuts (DÉCISION) ;

- questions diverses des adhérents.
