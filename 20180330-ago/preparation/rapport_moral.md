Rapport moral 2018
==================

Le rapport moral de cette année couvre toute la période de la création
de l'association à la présente assemblée générale.

L'association a été officiellement créée (publication au journal
officiel) le 18 mars 2017. Suite à cette publication, le collectif a
subit une baisse d'activité assez conséquente, conduisant d'ailleurs au
départ de quelques membres historiques.

Sous l'impulsion de Sacha et Vinilox, les travaux ont été relancés à
partir de mi-août. Un serveur de récupération a été installé et
configuré pour accueillir Nextcloud. Étant dans l'impossibilité dans
l'immédiat d'installer le serveur dans la baie de FAImaison, il a été
décidé de temporairement faire de l'hébergement chez un membre
FAImaison.

Le lancement des premiers services orientés autour de Nextcloud a eu
lieu début septembre.

Le choix de financement de ces premiers services et le mode d'adhésion
pour les membres ont été les suivants :

- d'une part l'adhésion, qui permet aux membres de s'impliquer dans
  l'association — être invité aux AGs, participer aux réunions
  décisionnelles, … — et de bénéficier de l'accès aux services les moins
  gourmands en ressources. L'adhésion a été fixée à 20€ par an ;
- d'autre part un abonnement à payer à part pour les services plus
  intensifs — pour l'instant Nextcloud seul —, sous la forme d'un prix
  libre, avec un minimum de 6€ par mois.

Cette dynamique de lancement des services et l'ouverture des adhésion a
permis la constitution d'un noyau dur de membres, constitués *de facto*
en Matou, selon nos statuts. Notons en particulier le travail de suivi
sur la comptabilité effectué par Kolbo5 depuis. Le compte en banque de
l'association a été ouvert le 6 octobre.

Octobre a été l'occasion d'ajouter d'autres services de fond et en
particulier les listes de diffusions, dont certaines ont remplacées les
boîtes de courriers électroniques institutionnelles.

L'automne a également été l'occasion de mettre en œuvre le cycle de
fonctionnement de l'association avec deux rendez-vous par mois, à
savoir :

- une « rencontre », dédiée à l'accueil des nouveaux, à l'échange entre
  membres, à la formation sur l'utilisation de nos outils ;
- une « réunion », dédiée au strict suivi associatif et administratif :
  vérification des comptes, décision sur les services à ouvrir,
  répartition des tâches à faire…

Malgré les bonnes volontés, les rencontres sont restées peu ouvertes sur
l'extérieur et quasi aucun nouveau membre n'est entré dans l'association
par ce biais. Depuis décembre, ce format de rendez-vous est
factuellement abandonné. Seules les « réunions » subsistent et ont été
tenues régulièrement.

Au début de l’hiver l’association a été invitée plusieurs fois à se
présenter lors de différents évènements : conférence gesticulée de
Lunar et sur un café vie privée nantais.

En décembre également, l'association a pu faire parler d'elle au travers
d'une intervention de Milouse dans une émission de radio diffusée sur
Prun'.

Autour de Noël, des réflexions ont été menés par certains membres sur la
refonte graphique et la communication au sens large de l'association,
que ce soit en interne entre adhérents, mais également à destination du
grand public. Différentes productions ont été réalisées à cette
occasion.

Janvier a été l'occasion pour les membres actifs de se pencher
sérieusement sur la question du financement de l'association, à la
lumière des quatre mois écoulés depuis l'ouverture des adhésions. Il est
apparu que les formules de financement évoquées précédemment étaient
compliquées à expliquer aux potentiel·les futur·es adhérent·es et
pouvaient même desservir l'association. Il a donc été convenu de les
abandonner pour les remplacer par une formule plus simple sous la forme
d'une adhésion de 20€, à laquelle s'associe une participation libre au
financement des services.

Un nouveau serveur plus récent, économique et écologique a été acquis
en début d'année afin de pouvoir disposer d'une base technique solide
pour les prochaines années. Des dépenses ont été engagées dans ce sens
pour l'acquisition d'un SSD et de barrettes de mémoire. Le serveur a
été rapidement installé, configuré et racké dans la baie de FAImaison
pour remplacer l'ancien serveur toujours hébergé chez un adhérent. De
nouveaux services pourront ainsi être aisément installés dans les mois
suivants.

Le 22 février, Milouse a participé au nom de Duchesse au forum du réseau
de l'environnement organisé par Écopôle à Nantes. Ce rassemblement avait
pour but de faire se rencontrer différents acteurs et actrices de la
métropole nantaise, tant association qu'entreprises ou pouvoirs publics,
pour s'interroger sur les impacts des activités numériques sur
l'environnement.

Depuis, une baisse d'activité s'est faite à nouveau ressentir. La
dernière réunion de l'association s'est rappelée qu'il était nécessaire
d'organiser l'assemblée générale avant le 8 avril : c'est chose faite.
