# Présents

- Sacha
- Milouse
- Vinilox
- Kolbo
- Fabien
- Greg
- Zeroleptik

# Réunion préparatoire

Une réunion préparatoire avait eu lieu le mercredi précédent en présence
de :

- Sacha
- Vinilox
- Milouse

Lors de cette réunion les points suivants avaient été abordés :

- Banque (rendez-vous nécessaire la semaine suivante)
- Validation des prix cotisations / services
- Tableur pour consigner les noms/prénoms/prends les services ou pas
- Utilisation d'un outil de ticketing

# Banque

Le choix s'est porté sur le Crédit Agricole

- Facilités (conseiller de Vinilox)
- Gratuit pour un compte avec chéquier / accès en ligne / etc… sans CB
- Probablement des assurances intéressantes

# Validation du prix

Cf. tableau en pièce jointe

Coût de fonctionnement sans services : 158€/an (dont DNS 32€,
inscription FAImaison 60€, budget com' 50€).

*Donc :* adhésion à 20€ pour avoir de la sécurité si 10 membres

## Postes de dépenses :

- Serveur : 50€/mois
- 1U 120€/mois chez FAImaison (avec décision de ne verser que 50 €
  mensuel pour le moment)
- 150€ de disque (une fois)

*Donc :* 6€/mois accès aux services

## Décisions :

- Prix 20€/an :                       6 pour / 0 contre / 0 abstention
- Prix services 6€/mois :             6 pour / 0 contre / 0 abstention

# Gestion de la motivation :

## Trésorerie

Il est déclaré le besoin d'un nouveau trésorier pour suppléer un poste
vacant de fait.

Tâches liées :
  - Entrées / sorties
  - Adhérents
  - Anticipation sur un an (AG : février)

Volontaire : **Kolbo**

## Ticketing

Mode de fonctionnement par ticket pour savoir où on en est, où en va,
qui fait quoi.

- Vision sur les tâches de l'association
- Utilisation de Gitlab : semble ok pour tout le monde, à surveiller dans
  le temps, ne pas faire « peur » aux nouveaux arrivants, utilisation de
  l'outil seulement pour les personnes s'impliquant.
- Un point est prévu après quelque mois de fonctionnement pour vérifier
  que l'usage de cet outil correspond à nos buts et minimise les effets
  néfastes.

## Se réunir

Les bases de la discussion sont les suivants :

- il serait bon que l'association soit fun et sexy. Pour ce faire le
  principal moyen est encore de faire des choses ensembles.
- rester ensemble le plus régulièrement possible pour faire des choses
- on doit rester efficace

Nous décidons d'organiser des réunions :

- Une fois par mois : une **réunion** de travail pour avancer :
  reprendre les tâches, ordre du jour : problématiques en cours,
  décisions, réaffecter les tâches, trésorerie.
  - Une durée maximum de 30 à 45 minutes. Une heure de fin précisée
    avant la réunion.
  - Pour la fin de la réunion, les tâches à réaliser sont affectées à
	quelqu'un et la rédaction du CR est terminée.

- Une fois par mois : une **rencontre** : une présentation ou débat
  (15-20min) présentée par quelqu'un sur un sujet donné connu à
  l'avance.  Point d'étape intermédiaire « cool » ainsi qu'un accueil
  convivial aux nouveaux.

Soit deux réunions par mois.

## Décisions

- Fonctionnement proposé avec 2 réunions/mois  : 6p/0a/0c
- 1 rencontre + 1 réunion : 6p/0a/0c

- Réunion : 1er mercredi/mois
   - les tâches décidées sont affectés en fin de réunion

- Rencontre : date à débattre -> à voir pour octobre (Vinilox)

- Cette rencontre peut être un greffage sur d'autre évènements,
  notamment à nos débuts.
- À confirmer dans les semaines suivantes, ces pistes sur
  l'organisation et le but de cette réunion :
  - une date fixe : (exemple : le 15 du mois)
  - type de sujet : installation de service / présentation de service,
    explications de lois/événement d'actualité (proposé par Milouse)

- Pas d'unanimité ni d'orientation clair sur le déroulement de ces rencontres.
- Lieu : confirmer côté ville de Nantes.

# Domaines

Domaines non-renouvellés : duches.se / duchesse.online
Le domaine restant est duchesse.chat.

# Lancement des services

Confirmation sur le lancement des services en septembre

# Mail de Lunar

L'association a reçu un mail dans la semaine sur la *mailing list*
provenant de Lunar, à propos d'une conférence gesticulé qu'il animera le
29 septembre à Nantes. Dans ce mail, Lunar nous propose d'intervenir en
fin de conférence pour présenter notre association et nos outils. Il
nous demande en sus si nous possédons des flyers que nous pourrions
distribuer à cette occasion.

Il est décidé Que Milouse répondra au mail par la positive. Un groupe
est formé pour créer les flyers.

L'idée est d'utiliser cette conférence gesticulée « comme si » il
s'agissait de la première « réunion cool » de l'asso.

# À faire

- Expliquer les todo : Sacha
- flyers - Fabien + Zeroleptik en support + Vinilox
- recontacter Lunar : Milouse
- organisation cafés vie privées et sondage salles (Vinilox)
- puis réserver salle (Milouse)
- création tableau des comptes (Kolbo)

# À noter

Présentation de l'association :

- le **29 septembre**, à Café vie privée (Salle Bretagne)
  https://informatique-ou-libertes.fr/
