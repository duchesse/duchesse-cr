Attention, ce CR est ici à titre informatif. À cette époque,
l'association duchesse n'avait pas été constituée.

Cette réunion s'est déroulée le mercredi 9 novembre à Pioche.

# Présents

- Jonathan
- Sacha
- Tcit
- CapsLock
- Yann
- Will
- lidstah
- Milouse
- Sora
- Opi (irc)
- Sky (irc)

# Déroulement Ordre du jour

## 20h00: accueil / nommage d'un scribe

 Lidstah se dévoue :)

## 20h15: rappels de ce que peuvent être les CHATONS

Court exposé de la proposition de framasoft

## 20h30: discussion sur la forme juridique de duchesse

Est-ce duchesse a vocation à être une association loi 1901, un groupe de
particuliers, etc.

Soit on reste un « collectif de gens » (problème de la collecte de
dons/financements/personnes privées, etc).

- Modèle associatif loi 1901:
  + présidence? (répartissable entre « co-présidents » par ex Framasoft)
  + pros:
    * souple
    * facile à mettre en place
    * cadre légal notamment au niveau des impôts
    * statut juridique (subventions notamment)
    * numéro SIRET si facturation de services
  + cons:
    * impôts si « bénéfices » non réinvestis dans l'asso
    * « fichage » des adhérents (voir ce qui est fait chez FAIMaison par
      exemple)
- personne morale (entreprise):
  + SCIC
  + SCOP
  + À penser en terme d'évolution une fois tout « construit » ?
  + cons:
    * on oublie les subventions

Exemple Framasoft: 5 salariés (2 CAE + CDDs, 5 CDIs à terme).

Dépend du modèle économique - pour le moment potentiellement informel ou
alors association 1901 ?

Doit on s'adresser à des personnes physiques et/ou personnes morales
(petites assos, TPE, etc.) ?

Plus simple au début de toucher des petites assos/structures (y compris
politiques) intéressées par ce genre de solution, que des particuliers ?

### Question

Est-ce que pour le moment (démarrage) on refuse au lancement de la
structure les personnes morales sans distinction de taille ?

7 votes contre sur 9

Mais ce n'est pas exclu pour l'avenir.

## 21h00: [DÉCISION] sur la forme juridique

Et par extension sur le positionnement d'un bureau éventuel

### Choix de la forme juridique

On pose le postulat que nous sommes en assemblée constituante

- entreprise : 1 vote pour sur les 10 présents
- un collectif informel : 0 vote sur les 10 présents
- assos loi 1901 : 10 votes pour sur les 10 présents

### Élection du président et du trésorier

Il est proposé d'élire le bureau de suite. Ce bureau aura en charge de
proposer des statuts, qui seront proposés sur la mailinglist. Ces seront
votés lors de l'assemblée générale constituante (qui se déroulera lors
de la prochaine réunion).

- Jonathan se propose au poste du président
- Milouse se propose au poste de trésorier

## 21h10: discussion sur le financement de duchesse

Adhésions, dons, subventions…

## 21h30: [DÉCISION] sur les modes de financements retenus

Pour aider à discuter : il y a actuellement une consommation de ~50€ en
numéraires, soit 30€ de vm chez faimaison par mois et 3 domaines ~10€
par an.

### Est-ce que les services sont payants ?

Sur les 11 présents :

- 5 votes pour
- 2 vote contre
- 4 votes blancs

### Fait-ont un prix libre ?

Sur les 11 présents :

- 5 votes pour
- 2 votes contres
- 3 votes blancs
- 1 abstention

### Doit-on doit être adhérent pour bénéficier des services ?

Sur les 11 présents :

- 3 votes pour
- 3 votes contres
- 5 votes blancs

### L'adhésion est-elle payante ?

Sur les 11 présents :

- 10 votes pour
- 0 vote contre
- 1 vote blanc

### Fait-on fait du prix libre pour l'adhésion ?

Sur les 11 présents :

- 5 votes pour
- 4 votes contres
- 2 votes blancs

### Prévoit-on une adhésion avec différents tarifs ?

Les tarifs suivants sont évoqués :

- particulier ;
- particulier avec des moyens limités ;
- association ;
- entreprise.

Sur les 10 présents (manque Opi) :

- 7 votes pour
- 1 votes contres
- 2 votes blancs

### S'autorise-t-on à faire appel à des subventions ?

Sur les 11 présents :

- 9 votes pour
- 0 vote contre
- 2 votes blancs

### Accepte-t-on les subventions (mécènat) du secteur privé ?

Sur les 11 présents :

- 1 votes pour
- 2  votes contres
- 8 votes blancs

# Accepte-t-on les subventions du secteur public ?

Sur les 11 présents :

- 9 votes pour
- 0 vote contre
- 2 votes blancs

### Envisage-t-on de pouvoir faire des appels aux dons ?

Sur les 11 présents :

- 11 votes pour
- 0 vote contre
- 0 vote blanc

### Certain appels au dons peuvent-ils être lancés uniquement en interne ?

Sur les 11 présents :

- 0 vote pour
- 8 votes contres
- 3 vote blanc

Cette question fait débat ; on est d'accord de l'aborder plus tard avec les statuts.

# PAUSE

# 22h55: discussion sur la forme des futures réunion

Et réflexion sur le planning de réunion prévisionnel.

Une envie de se regrouper régulièrement, quel que soit la finalité du
regroupement. Pas fonctionner en silo.

Profiter de ces regroupements pour faire de la péda technique, acquérir
des compétences techniques.

# 23h10: [DÉCISION] sur le planning des réunions

Partir sur une réunion tous les 15 jours le mercredi. La prochaine
serait donc le 23 novembre.

Finalement première réunion technique la semaine prochaine, le mercredi
16 novembre.

# Discussion sur les services que l'on souhaite ouvrir

Pour info : les applications les plus utilisées chez framasoft :
- framadate
- framapad
- framacarte
- framindmap
- framacalc
- framabee

Liste des services que l'on souhaite installer :

- nextcloud (Milouse, Lidstah, Tcit, Sacha)
- diaspora (Milouse, Sacha, Lidstah, Will)
- php task force (Tcit, Sacha…)
