# Présent·es

- Milouse
- Kolbo5

# Déroulé de l'Assemblée Générale

L'Assemblée Générale Extraordinaire (AGE) débute à 17h50.

Milouse a procédé au rappel de l'ordre du jour, comme suit :

- Ouverture de l'Assemblée
- Dissolution de l'association
- Élection des liquidat·eur·rice·s (seulement si la dissolution est actée)

## Ouverture de l'Assemblée

Milouse rappelle que cette présente Assemblée a été convoquée suite à la
constatation d’absence d’activité de l’association lors de l’Assemblée
Générale Ordinaire tenue le même jour. En conséquence de quoi, la
dissolution de l’association est proposée.

## Dissolution de l’association

Au vu de l’absence d’activité de l’association et de la volonté des
membres de se dégager de leurs responsabilités, la dissolution de
l’association est proposée.

En cas de vote négatif, un nouveau bureau sera élu pour une année
supplémentaire et le montant de la cotisation pour la nouvelle année
devra être voté. En cas de vote positif, un liquidateur ou une
liquidatrice devra être élu, et une ou plusieurs associations choisie
pour répartir les actifs restant de l’association.

### Vote

La dissolution de l’association est soumise au vote

Sur les deux personnes présentes :

- 2 votes pour
- 0 vote contre, blanc ou abstention

La conclusion de ce vote appelle, à l’unanimité des présent·e·s, la
liquidation de l’association.

La suite de cette assemblée va donc être consacrée à l'identification
des liquidat·eur·ice·s et à la décision quant à la répartition des
actifs restant.

## Élection des liquidat·eur·rice·s

La composition du bureau existant est rappelée, d'après la déclaration
en préfecture. Les personnes suivantes en faisaient partie :

- Milouse
- Kolbo

Candidat·es pour la liquidation :

- Étienne Deparis (dit Milouse)
- Emmanuel Olivier (dit Kolbo5)

### Vote pour l'élection d'Étienne Deparis en tant que liquidateur

Sur les deux personnes présentes :

- 2 votes pour
- 0 vote contre, blancs ou abstentions

### Vote pour l'élection d'Emmanuel Olivier en tant que liquidateur

Sur les deux personnes présentes :

- 2 votes pour
- 0 vote contre, blanc ou abstention

### Nommage des liquidateurs

Sont donc désignés liquidateurs de l’association Étienne Deparis et
Emmanuel Olivier.

## Répartition des biens restant à l’association

Milouse propose de verser l’ensemble des actifs restant de Duchesse,
après paiement des sommes dûs restantes à Hetzner et Gandi et
possiblement remboursement de Gordon, à l’association FAIMaison de
Nantes.

### Vote sur le don des actifs restants à FAIMaison

Sur les deux personnes présentes :

- 2 votes pour
- 0 vote contre, blancs ou abstentions


# Clôture de l'Assemblée

L'Assemblée Générale Extraordinaire se termine à 18h37.
