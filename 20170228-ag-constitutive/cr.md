Le *28 février 2017 *à *19 heures*, les fondateurs de *l’association
Duchesse, CHATON Nantais* se sont réunis en assemblée générale
constitutive à Nantes.

Sont présents :

- CITHAREL Thomas, 13 Rue du Coudray, 44000 Nantes
- TRÉMOUREUX Sacha, 1 Rue du Port Garnier, 44000 Nantes
- RENAUDINEAU Yann, 26 Bis Rue du Général Bedeau, 44000 Nantes
- DEBARRE Vincent, 7 Rue Maurice Sibille, 44000 Nantes
- NICOLAS Damien, 111 rue du général Buat, 44000 Nantes
- VINCE Stéphane, 4 place du Croisic, 44000 Nantes
- GEFFROY Ghislain, 9 rue du Pâtis 44830 Brains
- MONNIER Haelwenn, 12 rue Bois Savary, 44600 Saint-Nazaire

L’assemblée générale désigne Sacha TRÉMOUREUX en qualité de président
de séance et Thomas CITHAREL en qualité de secrétaire de séance.

Le président de séance met à la disposition des présents le projet de
statuts de l’association et l’état des actes passés pour le compte de
l’association en formation.

Puis il rappelle que l’assemblée générale constitutive est appelée à
statuer sur l’ordre du jour suivant :

- présentation du projet de constitution de l’association ;
- présentation du projet de statuts ;
- adoption des statuts ;
- désignation des premiers membres du conseil ;
- pouvoirs en vue des formalités de déclaration et publication.

Enfin, le président expose les motifs du projet de création de
l’association et commente le projet de statuts.

Il ouvre la discussion. Un débat s’instaure entre les membres de
l’assemblée. Tous les membres sont d'accord avec le projet associatif
ainsi que les statuts.

Après quoi, personne ne demandant plus la parole, le président met
successivement aux voix les délibérations suivantes.

# 1ère délibération :

L’assemblée générale adopte les statuts dont le projet lui a été
soumis. Cette délibération est adoptée à l’unanimité.

# 2ème délibération :

L’assemblée générale constitutive désigne en qualité de premiers
membres du conseil.

 - CITHAREL Thomas Dominique Jacques François, français, 13 Rue du Coudray, 44000 Nantes, technicien informatique
 - TRÉMOUREUX Sacha, Freddy, français, 1 Rue du Port Garnier, 44000 Nantes, administrateur systèmes
 - RENAUDINEAU, Yann Eugène Marie, français 26 Bis Rue du Général Bedeau, 44000 Nantes, technicien informatique
 - DEBARRE Vincent, français, 7 rue Maurice Sibille, 44000 Nantes, étudiant en pharmacie

Conformément aux statuts, cette désignation est faite pour une durée
expirant lors de l’assemblée générale qui sera appelée à statuer sur
les comptes de l’exercice clos le 28 février 2017.

Les membres du conseil ainsi désignés acceptent leurs fonctions.


# 3ème délibération :

L'assemblée générale constitutive déclare le siège de l'association à
l'adresse suivante :

**Association Duchesse, 13 Rue du Coudray, 44000 Nantes**
