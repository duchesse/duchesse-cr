Attention, ce CR est ici à titre informatif. À cette époque,
l'association duchesse n'avait pas été constituée.

# Présents

	awk '{ print $3 }' 20161116-duchess.logs | sort -Ru | grep -Ev '^\*|<?\-\->?'

- sora
- gordon
- Vinilox
- will
- katsuo49
- CapsLock
- tcit
- arn858
- lanodan
- sacha

# Ordre du jour

L'objectif de ce soir est d'obtenir une première approche technique
afin de pouvoir commencer la mise en place de services et de s'appuyer
sur des bases validées par chacun.

# Méthodologie

## Interdits éthiques

L'utilisation de logiciels propriétaires est exclue.

## Maintien d'un historique des modifications du serveur

Un historique de la configuration de chaque machine devra être stocké
sur Git grâce à l'utilisation d'etckeeper.

## Gestion de la configuration

Afin de maintenir une configuration reproductible, des recettes
Ansible seront développées par Duchesse. Le choix de cet outil s'est
fait par l'expérience de chacun.

## Mise en place d'un environnement d'apprentissage

Afin de permettre aux débutants de s'entraîner aux pratiques de
l'administration système, un environnement d'apprentissage sera mis en
place. De plus, cet environnement sera une étape obligatoire pour
n'importe quel service afin de partir en production.


Le déploiement d'un service devra suivre les étapes suivantes :


- Configuration sur l'environnement d'apprentissage
- Tests de l'application
- Recette avec Ansible
- Validation du playbook (suite d'opérations) Ansible
- Après validation, déploiement en production

## Rédaction d'un guide d'accueil

Un accent a été porté sur la transmission de connaissances. Pour
faciliter l'intégration de nouveaux membres et encourager les actions
techniques, un guide d'accueil sera redigé.

## Mise en place d'un wiki

Une base de connaissances pour l'ensemble de l'associtaion sera mise
en place. Le choix technique s'oriente vers Dokuwiki.

## Accès aux machines virtuelles

Deux machines virtuelles seront déployées dans un premier temps :

- une machine virtuelle de production
- une machine virtuelle « laboratoire, essais, rédaction de playbooks
».

Tout membre le souhaitant pourra obtenir un accès à la machine
laboratoire.

Un noyau restreint de membres se faisant confiance aura un accès à la
production. Le noyau pourra intégrer de nouveaux administrateurs selon
les modalités suivantes :

- observation du « candidat » par le noyau pour observer sa
  motivation. Pas de procédure stricte mais plutôt une attention
  portée sur ce que le « candidat » a pu produire en amont (nouveau
  service, documentation, recettage, etc…).
- Un système de cooptation pourra être mis en place.
- L'accès doit être nécessaire pour la tâche à réaliser.

Un accès peut être révoqué à partir du moment où :

- le titulaire en fait la demande
- le titulaire n'est plus actif dans Duchesse depuis un an
- le titulaire est radié
- le titulaire a réalisé des actions dommageables sur la production

## Gestionnaire de tickets et de tâches

Le code produit par l'équipe technique sera stocké dans un
Gitlab. Cette solution a été approuvée du fait de la possibilité de
gérer des issues ainsi qu'un board Kanban au sein du même outil.

L'utilisation de Pads n'est pas exclue.

# Choix techniques
## Gestion des zones DNS

À moyen terme, la gestion des serveurs DNS sera internalisée sur les
serveurs de Duchesse.

## Serveurs web

Un consensus s'est établi autour de l'utilisation de Nginx comme
serveur web principal.

## Utilisation de conteneurs

Après un large débat, les participants ont pour majorité rejeté
l'utilisation de Docker pour les difficultés de mise en
production. Afin de conserver pour autant une isolation des
applications, l'utilisation de LXC comme solution de conteneurisation
intéresse l'équipe technique.

# Tâches à effectuer

Les tâches non-techniques suivantes ont été décidées :

- Rédaction d'un code de conduite des admin sys. Ce dernier pour
  être modifié par la suite en usant d'amendements soumis à vote si
  des changements doivent être operés.
- Écriture sur le wiki du fonctionnement de l'équipe technique
  Duchesse.
- Rédaction d'un début de livret d'accueil.

# Prochaine réunion technique

Vendredi 25 novembre, 20 heures sur IRC : irc.geeknode.org, canal \#duchesse

# Annexe : Idées de noms d'hôtes

- croquettes.duchesse.chat
- litière.duchesse.chat
- boulettes.duchesse.chat
- chat.duchesse.chat
- chaussettes.archi.duches.se
